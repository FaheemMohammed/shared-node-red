## [1.0.4](https://gitlab.com/obi-dbs/dataprotectionapplication/shared-nodes/compare/v1.0.3...v1.0.4) (2023-10-25)

## [1.0.3](https://gitlab.com/obi-dbs/dataprotectionapplication/shared-nodes/compare/v1.0.2...v1.0.3) (2023-10-20)

## [1.0.2](https://gitlab.com/obi-dbs/dataprotectionapplication/shared-nodes/compare/v1.0.1...v1.0.2) (2023-10-19)

## [1.0.1](https://gitlab.com/obi-dbs/dataprotectionapplication/shared-nodes/compare/v1.0.0...v1.0.1) (2023-10-18)
