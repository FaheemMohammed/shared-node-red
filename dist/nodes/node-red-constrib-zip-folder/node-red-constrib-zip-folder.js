const nodeInit = (RED) => {
    function NodeRedConstribZipFolderNodeConstructor(config) {
        RED.nodes.createNode(this, config);
        console.log(config);
        // Declare the zip function
        let zip;
        this.on('input', async (msg, send, done) => {
            let message = msg;
            const sourcePath = config.sourcePath || message.sourcePath;
            const targetPath = config.targetPath || message.targetPath;
            const filename = config.filename || message.filename;
            try {
                const zipAFolderModule = await import('zip-a-folder');
                zip = async () => {
                    try {
                        await zipAFolderModule.zip(sourcePath, `${targetPath}${filename}.${config.compression.replace('compression-', '')}`);
                        send(msg);
                        done();
                    }
                    catch (error) {
                        console.error('Error during zip operation:', error);
                    }
                };
            }
            catch (error) {
                console.error('Failed to import zip-a-folder:', error);
            }
            zip && zip(sourcePath, targetPath);
        });
    }
    RED.nodes.registerType('node-red-constrib-zip-folder', NodeRedConstribZipFolderNodeConstructor);
};
module.exports = nodeInit;
//# sourceMappingURL=node-red-constrib-zip-folder.js.map