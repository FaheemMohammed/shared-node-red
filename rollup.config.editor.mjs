import fs from "fs";
import glob from "glob";
import path from "path";
import typescript from "@rollup/plugin-typescript";

import packageJson from "./package.json" assert { type: 'json' };

const allNodeTypes = Object.keys(packageJson["node-red"].nodes);

const makePlugins = (nodeType) => [
  typescript({
    lib: ["es5", "es6", "dom"],
    include: [
      `src/nodes/${nodeType}/${nodeType}.html/**/*.ts`,
      `src/nodes/${nodeType}/shared/**/*.ts`,
      "src/nodes/shared/**/*.ts",
    ],
    target: "es5",
    tsconfig: false,
    noEmitOnError: process.env.ROLLUP_WATCH ? false : true,
  }),
];

const makeConfigItem = (nodeType) => {
  const input = `src/nodes/${nodeType}/${nodeType}.html/index.ts`;
  const output = {
    file: `dist/nodes/${nodeType}/${nodeType}.html`,
    format: "iife",
  };

  // Include your HTML content as a string in the generated JavaScript
  const htmlFiles = glob.sync(`src/nodes/${nodeType}/${nodeType}.html/*.html`);
  const htmlContents = htmlFiles.map((fPath) => fs.readFileSync(fPath, 'utf8')).join("\n");

  return {
    input,
    output,
    plugins: [
      {
        name: "htmlBundle",
        renderChunk(code, _chunk, _options) {
          // Inject the HTML content as a string into the JavaScript code
          code = `<script type="text/javascript">\n${code}\n</script>\n${htmlContents}\n`;
          return {
            code,
            map: { mappings: "" },
          };
        },
      },
      ...makePlugins(nodeType),
    ],
    watch: {
      clearScreen: false,
    },
  };
};

export default allNodeTypes.map((nodeType) => makeConfigItem(nodeType));
