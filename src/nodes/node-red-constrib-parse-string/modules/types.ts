import { Node, NodeDef } from "node-red";
import { NodeRedConstribParseStringOptions } from "../shared/types";

export interface NodeRedConstribParseStringNodeDef extends NodeDef, NodeRedConstribParseStringOptions {}

// export interface NodeRedConstribParseStringNode extends Node {}
export type NodeRedConstribParseStringNode = Node;
