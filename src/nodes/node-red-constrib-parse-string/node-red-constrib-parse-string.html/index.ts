import { EditorRED } from "node-red";
import { NodeRedConstribParseStringEditorNodeProperties } from "./modules/types";

declare const RED: EditorRED;

RED.nodes.registerType<NodeRedConstribParseStringEditorNodeProperties>("node-red-constrib-parse-string", {
  category: "function",
  color: "#a6bbcf",
  defaults: {
    name: { value: "" },
  },
  inputs: 1,
  outputs: 1,
  icon: "file.png",
  paletteLabel: "node red constrib parse string",
  label: function () {
    return this.name || "node red constrib parse string";
  },
});
