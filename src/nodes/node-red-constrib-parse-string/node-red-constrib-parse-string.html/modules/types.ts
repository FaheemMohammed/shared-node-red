import { EditorNodeProperties } from "node-red";
import { NodeRedConstribParseStringOptions } from "../../shared/types";

export interface NodeRedConstribParseStringEditorNodeProperties
  extends EditorNodeProperties,
    NodeRedConstribParseStringOptions {}
