import { NodeInitializer } from "node-red";
import { NodeRedConstribParseStringNode, NodeRedConstribParseStringNodeDef } from "./modules/types";

const nodeInit: NodeInitializer = (RED): void => {
  function NodeRedConstribParseStringNodeConstructor(
    this: NodeRedConstribParseStringNode,
    config: NodeRedConstribParseStringNodeDef
  ): void {
    RED.nodes.createNode(this, config);

    this.on("input", (msg, send, done) => {
      send(msg);
      done();
    });
  }

  RED.nodes.registerType("node-red-constrib-parse-string", NodeRedConstribParseStringNodeConstructor);
};

export = nodeInit;
