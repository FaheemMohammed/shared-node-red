import { Node, NodeDef, NodeMessageInFlow } from "node-red";
import { NodeRedConstribZipFolderOptions } from "../shared/types";

export interface NodeRedConstribZipFolderNodeDef extends NodeDef, NodeRedConstribZipFolderOptions { }

// export interface NodeRedConstribZipFolderNode extends Node {}
export type NodeRedConstribZipFolderNode = Node;

export interface nodeMessage extends NodeMessageInFlow {
    sourcePath: string;
    targetPath: string;
    filename: string;
}

