import { EditorRED } from 'node-red';

import { NodeRedConstribZipFolderEditorNodeProperties } from './modules/types';

declare const RED: EditorRED;

RED.nodes.registerType<NodeRedConstribZipFolderEditorNodeProperties>(
  'node-red-constrib-zip-folder',
  {
    category: 'function',
    color: '#a6bbcf',
    defaults: {
      name: { value: '' },
      filename: { value: '' },
      targetPath: { value: '' },
      sourcePath: { value: '' },
      compression: { value: '' },
    },
    inputs: 1,
    outputs: 1,
    icon: 'file.png',
    paletteLabel: 'zip folder',
    label: function () {
      return this.name || 'zip folder';
    },
  }
);
