import { EditorNodeProperties } from "node-red";
import { NodeRedConstribZipFolderOptions } from "../../shared/types";

export interface NodeRedConstribZipFolderEditorNodeProperties
  extends EditorNodeProperties,
    NodeRedConstribZipFolderOptions {}
