import { NodeInitializer } from 'node-red';
import {
  nodeMessage,
  NodeRedConstribZipFolderNode,
  NodeRedConstribZipFolderNodeDef,
} from './modules/types';

const nodeInit: NodeInitializer = (RED) => {
  function NodeRedConstribZipFolderNodeConstructor(
    this: NodeRedConstribZipFolderNode,
    config: NodeRedConstribZipFolderNodeDef
  ): void {
    RED.nodes.createNode(this, config);
    console.log(config);

    // Declare the zip function
    let zip: (sourcePath: string, targetPath: string) => Promise<void>;

    this.on('input', async (msg, send, done) => {
      let message = msg as nodeMessage;
      const sourcePath = config.sourcePath || message.sourcePath;
      const targetPath = config.targetPath || message.targetPath;
      const filename = config.filename || message.filename;

      try {
        const zipAFolderModule = await import('zip-a-folder');
        zip = async () => {
          try {
            await zipAFolderModule.zip(
              sourcePath,
              `${targetPath}${filename}.${config.compression.replace(
                'compression-',
                ''
              )}`
            );
            send(msg);

            done();
          } catch (error) {
            console.error('Error during zip operation:', error);
          }
        };
      } catch (error) {
        console.error('Failed to import zip-a-folder:', error);
      }
      zip && zip(sourcePath, targetPath);
    });
  }

  RED.nodes.registerType(
    'node-red-constrib-zip-folder',
    NodeRedConstribZipFolderNodeConstructor
  );
};

export default nodeInit;
