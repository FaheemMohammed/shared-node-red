export interface NodeRedConstribZipFolderOptions {
  filename: string;
  sourcePath: string;
  targetPath: string;
  compression: string;
}
