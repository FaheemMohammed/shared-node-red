const fs = require('fs');
const path = require('path');

function replaceExports(filePath) {
  const content = fs.readFileSync(filePath, 'utf8');
  const replacedContent = content.replace(/export default/g, 'module.exports =');
  fs.writeFileSync(filePath, replacedContent);
}

function traverseDirectory(dirPath) {
  const files = fs.readdirSync(dirPath);

  for (const file of files) {
    const filePath = path.join(dirPath, file);
    if (fs.statSync(filePath).isDirectory()) {
      traverseDirectory(filePath);
    } else if (file.endsWith('.js') && filePath.includes('dist')) {
      replaceExports(filePath);
    }
  }
}

// Replace exports in all .js files within the 'dist' directory
traverseDirectory('./');
